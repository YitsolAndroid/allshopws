var server = require('../../server/server');

module.exports = function (server) {

  var remotes = server.remotes();

  remotes.after('**', function (ctx, next) {
    ctx.result = {
      status: "success",
      message: "",
      data: ctx.result
    };
    next();
  });

  remotes.afterError('**', function (ctx, next) {
    ctx.result = {
      status: "failure",
      message: ctx.error.code,
      data: ctx.result
    };
    next();
  })};


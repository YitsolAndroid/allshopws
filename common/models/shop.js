'use strict';
var asyncLoop = require('node-async-loop');

module.exports = function (Shop) {
    Shop.observe('before save', function (ctx, next) {
        var _Id = generateRandomString();
        ctx.instance.shopLicNum = _Id;
        if (ctx.isNewInstance) {
            ctx.instance['createdDate'] = formatDate(new Date());
            next();
        }
        else {
            next();
        }
    });
    //..........................................
    Shop.status = function (cb) {
        var currentDate = new Date();
        var currentHour = currentDate.getHours();
        var OPEN_HOUR = 6;
        var CLOSE_HOUR = 18;
        // console.log('Current hour is %d', currentHour);
        var response;
        if (currentHour >= OPEN_HOUR && currentHour < CLOSE_HOUR) {
            response = 'We are open for business.';
        } else {
            response = 'Sorry, we are closed. Open daily from 6am to 6pm.Look for other place';
        }
        cb(null, response);
    };
    //...................................
    Shop.remoteMethod(
        'status', {
            http: {
                path: '/status',
                verb: 'get'
            },
            returns: {
                arg: 'status',
                type: 'string'
            }
        }
    );
    //..........................

    Shop.getOwnerName = function (shopId, cb) {
        Shop.findById(shopId, function (err, instance) {
            var response = "Shop Owner Name is " + instance.shopOwnerName;
            cb(null, response);
            console.log(response);
        });
    }
    //........................................
    Shop.remoteMethod('getOwnerName', {
        http: { path: '/getShopOwnerName', verb: 'get' },
        accepts: { arg: 'id', type: 'string', http: { source: 'query' } },
        returns: { arg: 'result', type: 'string' }
    });
    //.....................................
    Shop.timeOutTest = function (cb) {
        var response = "timeout immediate example";
        console.log("first log");
        setImmediate(function () {
            console.log("second log");
        });
        console.log("third log");
        cb(null, response);
    };
    //.............................................
    Shop.remoteMethod('timeOutTest', {
        http: {
            path: '/timeOutTest',
            verb: 'get'
        },
        returns: {
            arg: 'status',
            type: 'string'
        }
    });
    //..............................................
    Shop.loopexample = function (err, cb) {
        var response = [];
        var obj = { 'aa': 'AAAA', 'bb': 'BBBB', 'cc': 'CCCC', 'dd': 'DDDD', 'ee': 'EEEE' };
        asyncLoop(obj, function (item, next) {
            response.push({ key: item.key, value: item.value });
            next();
        }, function () {
            console.log('Finished');
        });
        // for(var data in obj){
        //     response.push({key : data, value : obj[data]});
        // }

        cb(err, response);
    };
    //..................................................
    Shop.remoteMethod('loopexample', {
        http: {
            path: '/loopexample',
            verb: 'get'
        },
        returns: {
            arg: 'status',
            type: 'string'
        }
    });

};

//...................
function generateRandomString() {
    var result = '';
    var IDLIST = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
    var RANDOM_STRING_LENGTH = 8;
    for (var i = 0; i < RANDOM_STRING_LENGTH; i++) {
        result += IDLIST.charAt(Math.floor((Math.random() * IDLIST.length) + 1));
    }
    return result;
}
//..............................
function formatDate(date) {
    var monthNames = [
        "01", "02", "03",
        "04", "05", "06", "07",
        "08", "09", "10",
        "11", "12"
    ];

    var day = date.getDate();
    var monthIndex = date.getMonth();
    var year = date.getFullYear();

    if (monthIndex < 10) {
        day = "0".concat(day);
    }

    return day + '/' + monthNames[monthIndex] + '/' + year;
}



